subroutine pcg(iter, n, eps, rho, pot)
    use laplacian_natan_kronik, only : drct_aa, drct_bc, nord2
    use laplacian_natan_kronik, only : drct_bb, drct_cc
    use laplacian_natan_kronik, only : output_prefactor
    use laplacian_natan_kronik, only : output_stencil_coefficients
    implicit none
    !------------------------------------------------------------------------
    ! --- pass in variables ---
    !------------------------------------------------------------------------
    real(8), parameter   :: pi = acos(-1.d0)
    integer, intent(out) :: iter
    integer, intent(in)  :: n(3)
    real(8), intent(in)  :: eps
    real(8), intent(in)  :: rho(n(1),n(2),n(3))
    real(8), intent(out) :: pot(n(1),n(2),n(3))
    !------------------------------------------------------------------------

    !------------------------------------------------------------------------
    ! --- local variables ---
    !------------------------------------------------------------------------
    integer                   :: itr
    integer                   :: npt
    integer                   :: nd(6)
    integer                   :: nb(6)
    real(8)                   :: nro
    real(8)                   :: nr
    real(8)                   :: mnr
    real(8)                   :: alfa
    real(8)                   :: beta
    real(8), allocatable      :: x(:,:,:)
    real(8), allocatable      :: r(:,:,:)
    real(8), allocatable      :: z(:,:,:)   ! z = m-1 r
    real(8), allocatable      :: d0(:,:,:)
    real(8), allocatable      :: d1(:,:,:)
    real(8)                   :: fbsscale
    !------------------------------------------------------------------------
    real(8), external :: ddot
    !
    real(8) :: prefac_(drct_aa:drct_bc)
    real(8) :: stencil_coe(-nord2:nord2,drct_aa:drct_bc)
    real(8) :: stencil_coe_L(-nord2:nord2,drct_aa:drct_bc)
    !
    ! prepare derivatives
    call output_prefactor(prefac_)
    call output_stencil_coefficients(stencil_coe)
    stencil_coe = stencil_coe/(-4.d0*pi) ! add the -1/4pi to stencil_coe

    !------------------------------------------------------------------------
    ! --- allocate arrays ---
    !------------------------------------------------------------------------
    !
    nd(1)=1                                      
    nd(2)=1         
    nd(3)=1         
    nd(4)=n(1)      
    nd(5)=n(2)      
    nd(6)=n(3)      
    !
    nb(1)=1-nord2   
    nb(2)=1-nord2   
    nb(3)=1-nord2   
    nb(4)=n(1)+nord2
    nb(5)=n(2)+nord2
    nb(6)=n(3)+nord2
    !
    npt=(nb(4)-nb(1)+1)*(nb(5)-nb(2)+1)*(nb(6)-nb(3)+1)
    !
    allocate( x(nb(1):nb(4), nb(2):nb(5), nb(3):nb(6)))
    allocate( r(nb(1):nb(4), nb(2):nb(5), nb(3):nb(6)))
    allocate( z(nb(1):nb(4), nb(2):nb(5), nb(3):nb(6)))
    allocate(d0(nb(1):nb(4), nb(2):nb(5), nb(3):nb(6)))
    allocate(d1(nb(1):nb(4), nb(2):nb(5), nb(3):nb(6)))
    !
    ! erho
    call set_boundary_condition
    d1 = 0.d0
    call padx_new(nd,nb,prefac_,stencil_coe,x,d1)
    !
    r=0.0d0; x=0.0d0; z=0.0d0; d0=0.0d0
    !r(nd(1):nd(4),nd(2):nd(5),nd(3):nd(6))=rho(:,:,:)
    !x(nd(1):nd(4),nd(2):nd(5),nd(3):nd(6))=pot(:,:,:)
    r(1:n(1),1:n(2),1:n(3))=rho(:,:,:) ! TODO: memcp in the future...
    x(1:n(1),1:n(2),1:n(3))=pot(:,:,:) ! TODO: memcp in the future...
    !
    !erho part 2
    r(1:n(1),1:n(2),1:n(3)) = r(1:n(1),1:n(2),1:n(3)) - d1(1:n(1),1:n(2),1:n(3))  !erho part 2
    x = 0.d0; d1 = 0.d0
    x(1:n(1),1:n(2),1:n(3)) = pot(:,:,:) ! TODO: memcp in the future...
    !
    call load_preconditioner
    !
    call pcg_kernel_fast
    !call pcg_kernel_slow

    !------------------------------------------------------------------------
    ! print error
    !------------------------------------------------------------------------
    !r(nd(1):nd(4),nd(2):nd(5),nd(3):nd(6))=rho(:,:,:)
    !call padx_new(nd,nb,prefac_,stencil_coe,x,d0)
    !call daxpy(npt,-1.d0,d0,1,r,1)
    !------------------------------------------------------------------------
    ! WRITE(*,"(A, E15.7, A, I4, A)") "error: ", DSQRT(PDDOT(nd,nb,r,r)), "  in", iter, "  steps"
    !------------------------------------------------------------------------

    pot(:,:,:)=x(1:n(1),1:n(2),1:n(3))

    DEALLOCATE(x)
    DEALLOCATE(r)
    DEALLOCATE(z)
    DEALLOCATE(d0)
    DEALLOCATE(d1)
  contains

    subroutine  set_boundary_condition()
      use system, only : h, q_cen
      use grids, only : v_of_r
      implicit none
      integer :: ig1, ig2, ig3
      logical :: is_halo_1, is_halo_2, is_halo_3
      real(8) :: r_(3), r
      x = 0.d0
      do ig3 = nb(3), nb(6)
        is_halo_3 = .not.(ig3.ge.1.and.ig3.le.n(3))
        do ig2 = nb(2), nb(5)
          is_halo_2 = .not.(ig2.ge.1.and.ig2.le.n(2))
          do ig1 = nb(1), nb(4)
            is_halo_1 = .not.(ig1.ge.1.and.ig1.le.n(1))
            if (is_halo_1.or.is_halo_2.or.is_halo_3) then
              r_ = (/dble(ig1)/dble(n(1)),dble(ig2)/dble(n(2)),dble(ig3)/dble(n(3))/)
              r_ = matmul(h, r_)
              r = norm2(r_-q_cen)
              x(ig1,ig2,ig3) = v_of_r(r)
            end if
          end do ! ig1
        end do ! ig2
      end do ! ig3
      return
    end subroutine set_boundary_condition

    subroutine  load_preconditioner()
      implicit none
      integer :: u_l
      integer :: ingh
      !TODO
      stencil_coe_L = 0.d0
      ! step 1: load L.dat
      open(newunit=u_l, file='L_coe.dat', action='read')
      read(u_l,*) stencil_coe_L(0,drct_aa)
      do ingh = 1, nord2
        read(u_l,*) stencil_coe_L(ingh,drct_aa)
        stencil_coe_L(ingh,drct_bb) = stencil_coe_L(ingh,drct_aa) ! only for cubic case...
        stencil_coe_L(ingh,drct_cc) = stencil_coe_L(ingh,drct_aa) ! only for cubic case...
        ! symmetrize (for use of L and L')
        stencil_coe_L(-ingh,drct_aa) = stencil_coe_L(ingh,drct_aa)
        stencil_coe_L(-ingh,drct_bb) = stencil_coe_L(ingh,drct_aa)
        stencil_coe_L(-ingh,drct_cc) = stencil_coe_L(ingh,drct_aa)
      end do ! ingh
      close(u_l)
      ! step 2: get scale (i.e., 1/diagonal_stencil^2) to avoid division in fwdsub/bkdsub
      fbsscale = 1.d0/(stencil_coe_L(0,drct_aa)**2)
      stencil_coe_L = stencil_coe_L /(stencil_coe_L(0,drct_aa))
      !print *, stencil_coe_L(:,1)
      !print *, stencil_coe_L(:,2)
      !print *, stencil_coe_L(:,3)
      !stop
      return
    end subroutine load_preconditioner

    subroutine  pcg_kernel_fast()
      implicit none
      call padx_new(nd,nb,prefac_,stencil_coe,x,d1)
      call daxpy(npt,-1.0d0,d1,1,r,1)
      call dcopy(npt,r,1,d0,1)
      call dscal(npt,fbsscale,d0,1)
      call fwdsub(nd,nb,stencil_coe_L,d0)
      call bkdsub(nd,nb,stencil_coe_L,d0)
      call dcopy(npt,d0,1,z,1)
      !------------------------------------------------------------------------
      ! --- main iteration ---
      !------------------------------------------------------------------------
      iter=0
      nro = Ddot(npt,r,1,r,1)
      !
      DO itr=0,1000
        !                                       !       OP     PCG    FLOPS      MEM         DEP
        iter=iter+1                             ! ------------------------------------------------
        nr = Ddot(npt,r,1,r,1)                  ! [01] DOT              2N       2N
        print *,'cg: iter=',iter, 'nr =', nr
        IF (nr < eps*eps*(1+nro)) EXIT          !
        call padx_new(nd,nb,prefac_,stencil_coe,d0,d1) ! [02] mv              37n       3n
        mnr=Ddot(npt,r,1,z,1)                   ! [03] DOT      *       2N       2N
        alfa=mnr/Ddot(npt,d0,1,d1,1)            ! [04] DOT              2N       2N        02,03
        call daxpy(npt,alfa,d0,1,x,1)           ! [05] AXPY             2N       3N        04
        call daxpy(npt,-alfa,d1,1,r,1)          ! [06] AXPY             2N       3N        04
        call dcopy(npt,r,1,d1,1)                ! [07] CP               --       2N        06
        call dscal(npt,fbsscale,d1,1)           ! [08] SCAL     *        N       2N        07
        call fwdsub(nd,nb,stencil_coe_L,d1)     ! [09] FW       *      18N       2N        08
        call bkdsub(nd,nb,stencil_coe_L,d1)     ! [10] BW       *      18N       2N        09
        call dcopy(npt,d1,1,z,1)                ! [11] CP       *       --       2N        10
        beta=Ddot(npt,r,1,d1,1)/mnr             ! [12] DOT              2N       2N        09
        call daxpy(npt,beta,d0,1,d1,1)          ! [13] AXPY             2N       3N        12
        nro = nr
        !
        iter=iter+1
        nr = Ddot(npt,r,1,r,1)
        print *,'cg: iter=',iter, 'nr =', nr
        IF (nr < eps*eps*(1+nro)) EXIT
        call padx_new(nd,nb,prefac_,stencil_coe,d1,d0)
        mnr=Ddot(npt,r,1,z,1)
        alfa=mnr/Ddot(npt,d1,1,d0,1)
        call daxpy(npt,alfa,d1,1,x,1)
        call daxpy(npt,-alfa,d0,1,r,1)
        call dcopy(npt,r,1,d0,1)
        call dscal(npt,fbsscale,d0,1)
        call fwdsub(nd,nb,stencil_coe_L,d0)
        call bkdsub(nd,nb,stencil_coe_L,d0)
        call dcopy(npt,d0,1,z,1)
        beta=Ddot(npt,r,1,d0,1)/mnr
        call daxpy(npt,beta,d1,1,d0,1)
        nro = nr
        !
      END DO
      !------------------------------------------------------------------------
      return
    end subroutine pcg_kernel_fast

    subroutine  pcg_kernel_slow()
      implicit none
      ![01] r_0 = b - A . x_0
      call padx_new(nd,nb,prefac_,stencil_coe,x,d1) ! d1 <- A . x_0
      call daxpy(npt,-1.0d0,d1,1,r,1)               ! r <- r - d1 = b - A . x_0  (r = r_0)
      ![02] z_0 = M^-1. r_0
      call dcopy(npt,r,1,d0,1)                      ! d0 <- r = r_0
      call dscal(npt,fbsscale,d0,1)                 ! |
      call fwdsub(nd,nb,stencil_coe_L,d0)           ! |
      call bkdsub(nd,nb,stencil_coe_L,d0)           ! |: d0 <- M^-1 r_0 = z_0
      call dcopy(npt,d0,1,z,1)                      ! z <- d0 = z_0              (z = z_0)
      ![03] p_0 = z_0
      !                                             ! d0 is now also z_0         (d0 = p_0)
      ![04] k = 0
      iter=0                                        ! iter <- 0                  (iter = k)
      ![add] norm of initial residual
      nro = Ddot(npt,r,1,r,1)
      do while (.true.)
        ![05] alpha_k = (r_k . z_k)/(p_k . A . p_k)
        mnr=Ddot(npt,r,1,z,1)                          ! mnr <- r . z = r_k . z_k
        call padx_new(nd,nb,prefac_,stencil_coe,d0,d1) ! d1  <- A . d0 = A . p_k
        alfa=mnr/Ddot(npt,d0,1,d1,1)                   ! alfa <- r_k . z_k/(p_k . A . p_k);      (alfa = alpha_k)
        ![06] x_k+1 = x_k + alpha_k p_k
        call daxpy(npt,alfa,d0,1,x,1)                  ! x <- x + alfa*d0 = x_k + alpha_k*p_k;   (x = x_k+1)
        ![07] r_k+1 = r_k - alpha_k A . p_k
        call daxpy(npt,-alfa,d1,1,r,1)                 ! r <- r - alfa*d1 = r_k - alpha_k*A.p_k; (r = r_k+1)
        ![08] if (conv) exit
        nr = Ddot(npt,r,1,r,1)
        print *,'cg: iter=',iter, 'nr =', nr
        if (nr < eps*eps*(1+nro)) exit
        ![09] z_k+1 = M^-1. r_k+1
        call dcopy(npt,r,1,d1,1)                       ! d1 <- r = r_k+1
        call dscal(npt,fbsscale,d1,1)                  ! |
        call fwdsub(nd,nb,stencil_coe_L,d1)            ! |
        call bkdsub(nd,nb,stencil_coe_L,d1)            ! |: d1 <- M^-1 r_k+1 = z_k+1
        call dcopy(npt,d1,1,z,1)                       ! z <- d1 = z_k+1                         (z = z_k+1)
        ![10] beta_k = (z_k+1 . r_k+1)/(z_k . r_k)
        !                                              ! mnr = r_k . z_k
        beta=Ddot(npt,r,1,d1,1)/mnr                    ! beta <- r.d1/mnr = r_k+1 . z_k+1 /mnr;  (beta = beta_k)
        ![11] p_k+1 = z_k+1 + beta_k p_k
        !                                              ! d0 = p_k (need update for next step)
        call daxpy(npt,beta,d0,1,d1,1)                 ! d1 <- d1 + beta*d0 = z_k+1 + beta_k*p_k (d1 = p_k+1)
        call dcopy(npt,d1,1,d0,1)                      ! d0 <- d1                                (d0 = p_k+1)
        ![12] k = k+1
        iter = iter + 1
      end do
      !
      return
    end subroutine pcg_kernel_slow

end subroutine pcg
!============================================================================
