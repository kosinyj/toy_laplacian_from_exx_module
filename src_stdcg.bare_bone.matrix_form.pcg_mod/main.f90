!
! Author    : Hsin-Yu Ko
! Started at: 15.01.18
!
program main
  !use constants
  !
  implicit none
  call initialize ! get analytical potential at this point...
  call solve_for_potential
  call check_error_norm_with_exact_result
  call finalize
  stop
contains

  subroutine  initialize()
    use system, only : init_system, h, ainv
    use grids,  only : init_grids, dr1, dr2, dr3
    use laplacian_natan_kronik, only : init_derivatives
    implicit none
    call init_system
    call init_grids
    call init_derivatives(h, ainv, dr1, dr2, dr3)
    return
  end subroutine initialize

  subroutine  solve_for_potential()
    use grids, only : rho, pot_stdcg, pot, nr1, nr2, nr3, pot_pcg
    implicit none
    integer :: iter, n(3), u_cg, cg_mode
    real(8), parameter :: eps=1.0d-6
    open(newunit=u_cg, file='cg_mode.dat', action='read')
    read(u_cg,*) cg_mode
    close(u_cg)
    ! TODO apply boundary condition
    n(1) = nr1; n(2) = nr2; n(3) = nr3
    select case (cg_mode)
    case (1)
      call stdcg(iter, n, eps, rho, pot_stdcg,.true.)
    case (2)
      call pcg(iter, n, eps, rho, pot_stdcg)
    case (-1)
      call stdcg(iter, n, eps, rho, pot_stdcg,.false.)
    case default
      write(*,*) 'cg_mode not implemented...stop!'
      stop
    end select ! cg_mode
    return
  end subroutine solve_for_potential

  subroutine  check_error_norm_with_exact_result()
    use grids, only : pot, pot_stdcg
    use grids, only : nr1, nr2, nr3
    implicit none
    integer :: i,j,k
    print *, '|| v_exact ||_2 = ', norm2(pot)
    print *, '|| v_solved - v_exact ||_2 = ', norm2(pot_stdcg - pot)
    do k = 1, nr3
      do j = 1, nr2
        do i = 1, nr1
          write(100,*) pot_stdcg(i,j,k) , pot(i,j,k)
        end do ! i
      end do ! j
    end do ! k
    return
  end subroutine check_error_norm_with_exact_result

  subroutine  finalize()
    use grids, only : dealloc_grids
    implicit none
    call dealloc_grids
    return
  end subroutine finalize
end program main
